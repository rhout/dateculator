package dateculator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class DateCalculatorTest {
    @Test
    void sameDate() {
        DateCalculator fromDate = new DateCalculator("02/02/2000");
        DateCalculator toDate = new DateCalculator("02/02/2000");
        assertEquals(0, fromDate.daysUntil(toDate));
    }

    @Test
    void oneDaysDifference() {
        DateCalculator fromDate = new DateCalculator("02/02/2000");
        DateCalculator toDate = new DateCalculator("03/02/2000");
        assertEquals(1, fromDate.daysUntil(toDate));
    }

    @Test
    void oneDaysDifferenceNewYear() {
        DateCalculator fromDate = new DateCalculator("31/12/2021");
        DateCalculator toDate = new DateCalculator("01/01/2022");
        assertEquals(1, fromDate.daysUntil(toDate));
    }

    @Test
    void overlappingOneMonth() {
        DateCalculator fromDate = new DateCalculator("22/02/2020");
        DateCalculator toDate = new DateCalculator("03/03/2020");
        assertEquals(10, fromDate.daysUntil(toDate));
        fromDate = new DateCalculator("22/02/2022");
        toDate = new DateCalculator("03/03/2022");
        assertEquals(9, fromDate.daysUntil(toDate));
    }

    @Test
    void sameYear() {
        DateCalculator fromDate = new DateCalculator("02/02/2000");
        DateCalculator toDate = new DateCalculator("13/12/2000");
        assertEquals(315, fromDate.daysUntil(toDate));
    }

    @Test
    void differentYearsButLessThanAYear() {
        DateCalculator fromDate = new DateCalculator("17/09/2020");
        DateCalculator toDate = new DateCalculator("29/03/2021");
        assertEquals(193, fromDate.daysUntil(toDate));
    }

    @Test
    void lessThanAYearSameMonth() {
        DateCalculator fromDate = new DateCalculator("29/09/2020");
        DateCalculator toDate = new DateCalculator("17/09/2021");
        assertEquals(353, fromDate.daysUntil(toDate));
    }

    @Test
    void leapYear() {
        DateCalculator fromDate = new DateCalculator("02/02/2016");
        DateCalculator toDate = new DateCalculator("03/03/2016");
        assertEquals(30, fromDate.daysUntil(toDate));
        fromDate = new DateCalculator("02/02/2017");
        toDate = new DateCalculator("03/03/2017");
        assertEquals(29, fromDate.daysUntil(toDate));
    }

    @Test
    void manyYearsDifference() {
        DateCalculator fromDate = new DateCalculator("30/11/2011");
        DateCalculator toDate = new DateCalculator("17/06/2022");
        assertEquals(3852, fromDate.daysUntil(toDate));
    }

    @Test
    void endOfCenturyLeapYear() {
        DateCalculator fromDate = new DateCalculator("29/09/1701");
        DateCalculator toDate = new DateCalculator("17/09/2021");
        assertEquals(116866, fromDate.daysUntil(toDate));
    }

    @Test
    void toMonthIsJanuaryMoreThanAYear() {
        DateCalculator fromDate = new DateCalculator("01/01/2021");
        DateCalculator toDate = new DateCalculator("03/01/2022");
        assertEquals(367, fromDate.daysUntil(toDate));
        fromDate = new DateCalculator("01/01/2020");
        toDate = new DateCalculator("03/01/2021");
        assertEquals(368, fromDate.daysUntil(toDate));
    }
}
