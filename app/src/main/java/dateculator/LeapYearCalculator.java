package dateculator;

public class LeapYearCalculator {

    public static int addDayIfLeapYear(int year) {
        return isLeapYear(year) ? 1 : 0;
    }

    private static boolean isLeapYear(int year) {
        if (year % 400 == 0)
            return true;

        if (year % 100 == 0)
            return false;

        if (year % 4 == 0)
            return true;
        return false;
    }

    public static int leapYearsInRange(int yearFrom, int yearTo) {
        yearFrom--;
        int leapYearsToToYear = numberOfLeapYearsForYear(yearTo);
        int leapYearsToFromYear = numberOfLeapYearsForYear(yearFrom);
        return leapYearsToToYear - leapYearsToFromYear;
    }

    private static int numberOfLeapYearsForYear(int year) {
        return (year / 4) - (year / 100) + (year / 400);
    }
}
