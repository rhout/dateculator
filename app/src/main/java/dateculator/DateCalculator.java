package dateculator;

import java.util.Arrays;

public class DateCalculator {
    private static final int[] daysInMonth = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    private int date;
    private int month;
    private int year;

    public DateCalculator(String dateString) {
        String[] datePieces = dateString.split("/");
        date = getDatePartFromString(datePieces[0]);
        month = getDatePartFromString(datePieces[1]);
        year = getDatePartFromString(datePieces[2]);
    }

    private int getDatePartFromString(String datePart) {
        return Integer.parseInt(datePart);
    }

    public int daysUntil(DateCalculator toDate) {
        if (this.equals(toDate)) {
            return 0;
        }

        return looseDays(toDate) + daysInMonths(toDate) + daysInYears(toDate);
    }

    private int looseDays(DateCalculator toDate) {
        if (sameMonthsAndToDateAfterFromDate(toDate)) {
            return toDate.date - date;
        }
        return datesInFirstAndLastMonth(toDate);
    }

    private boolean sameMonthsAndToDateAfterFromDate(DateCalculator toDate) {
        return month == toDate.month && date < toDate.date;
    }

    private int daysInMonths(DateCalculator toDate) {
        if (month > toDate.month) {
            int daysInRestOfYear = calculateDaysInWholeMonthSpan(month, 12, year);
            year++;
            int daysInFirstOfYear = calculateDaysInWholeMonthSpan(1, toDate.month - 1, toDate.year);
            return daysInRestOfYear + daysInFirstOfYear;
        } else {
            return calculateDaysInWholeMonthSpan(month, toDate.month - 1, year);
        }
    }

    private int calculateDaysInWholeMonthSpan(int firstMonth, int lastMonth, int year) {
        int days = Arrays.stream(daysInMonth, firstMonth, lastMonth + 1).reduce(0,
                (subtotal, element) -> {
                    int daysToAdd = element;
                    if (daysToAdd == 28) {
                        daysToAdd += LeapYearCalculator.addDayIfLeapYear(year);
                    }
                    return subtotal + daysToAdd;
                });
        return days;
    }

    private int datesInFirstAndLastMonth(DateCalculator toDate) {
        int daysOfFirstMonth = daysInMonth[month] - date
                + (month == 2 ? LeapYearCalculator.addDayIfLeapYear(year) : 0);
        int daysOfLastMonth = toDate.date;
        if (month == 12) {
            month = 1;
            year++;
        } else {
            month++;
        }
        return daysOfFirstMonth + daysOfLastMonth;
    }

    private int daysInYears(DateCalculator toDate) {
        if (year != toDate.year) {
            return 365 * (toDate.year - year) + LeapYearCalculator.leapYearsInRange(year, toDate.year);
        }
        return 0;
    }

    @Override
    public boolean equals(Object compareObject) {
        if (compareObject == this) {
            return true;
        }
        if (!(compareObject instanceof DateCalculator)) {
            return false;
        }

        DateCalculator otherDate = (DateCalculator) compareObject;
        return date == otherDate.date && month == otherDate.month && year == otherDate.year;
    }

}
