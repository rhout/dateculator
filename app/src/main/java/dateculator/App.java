package dateculator;

import java.util.Scanner;

import org.checkerframework.checker.units.qual.s;

public class App {
    int[] daysInMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the first date (date from in the format dd/mm/yyyy)?");
        String inputDateFrom = scan.nextLine();

        DateCalculator fromDate = new DateCalculator(inputDateFrom);

        System.out.println("Enter the second date (date to in the format dd/mm/yyyy)?");
        String inputDateTo = scan.nextLine();
        DateCalculator toDate = new DateCalculator(inputDateTo);

        int daysBetweenDates = fromDate.daysUntil(toDate);
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder
                .append("There ")
                .append(daysBetweenDates == 1 ? "is " : "are ")
                .append(daysBetweenDates)
                .append(" day").append(daysBetweenDates == 1 ? "" : "s")
                .append(" between ")
                .append(inputDateFrom)
                .append(" and ")
                .append(inputDateTo);

        System.out.println(stringBuilder.toString());

        scan.close();
    }
}
