# :calendar: Dateculator :+1: 

 The magic machine that calculates how many days there are between two dates!

## Getting Started

Run the program and enter a from date and a to date in the format "dd/mm/yyyy". The program is very input sensitive, so if you write a wrong format, an invalid date or the first date is not before the second date, you will not get a good result, so please be cautious about your inputs until some validation has been made.

## Testing
The application has been tested with a number of [unit tests](app/src/test/java/dateculator/DateCalculatorTest.java)